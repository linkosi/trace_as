#!/bin/bash

cat /dev/null > /tmp/trace_as.txt

for arg in "$1"
do
    if [ "$arg" == "" ]; then
	echo "Invalid arguments"
	exit 1
    else
	ttl_Check=`ping $1 -c 1 | awk {'print $6'} | grep ttl | sed -e "s/ttl=//"`
    fi
done

for ((i = 1 ; i <= $ttl_Check ; i++))
do
    ip_Check=`ping $1 -c 1 -W 1 -t $i | awk {'print $2'} | sed -e '2!d'`
    echo ${ip_Check} | grep -v "bytes" | grep -v -e '^$' | tee -a /tmp/trace_as.txt > /dev/null
    if [ "$ip_Check" == "bytes" ]; then
    	break
    fi
done

while read -r line; do
    origin_Check=`whois $line | grep "^origin"`
    if [ "$?" == "1" ]; then
        rgx_Check=`echo $line | grep -e "192\.168\.[0-9]*\.[0-9]*" -e "10\.[0-9]*\.[0-9]*\.[0-9]*" --extended-regexp -e "172\.(1[6-9]|2[0-9]|3[01])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"`
	if [ "$?" == "0" ]; then
	    echo "$line: Private IP"
	else
	    echo "$line: N/A"
	fi
    else
        dlt_Origin=`whois $line | grep "^origin" | sed "s/origin:         //"`
        echo "$line: ${dlt_Origin}"
    fi
    echo -e ""
done < /tmp/trace_as.txt
